# HDFS traces

This dataset consists of some simple traces of the Hadoop Distributed File System.

The traces are primarily of read and write API calls to HDFS.

To visualize a trace, download the JSON file for the trace, then view it in [this](http://people.mpi-sws.org/~jcmace/xtrace/swimlane.html) visualization.